package org.zerock.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.zerock.domain.BoardVO;
import org.zerock.mapper.BoardMapper;

import lombok.RequiredArgsConstructor;
import lombok.ToString;
import lombok.extern.log4j.Log4j;

@Service
@RequiredArgsConstructor
@ToString
public class BoardServiceImpl implements BoardService {

	private final BoardMapper boardMapper;

	@Override
	public void register(BoardVO board) {
		boardMapper.insert(board);
		
	}
	
	@Override
	public Long registerSelectKey(BoardVO board) {
		boardMapper.insertSelectKey(board);
		
		return board.getBno();
		
	}
	
	@Override
	public BoardVO get(Long bno) {
		return boardMapper.read(bno);
	}

	@Override
	public int modify(BoardVO board) {
		
		return boardMapper.update(board);
	}

	@Override
	public int remove(Long bno) {
	
		return boardMapper.delete(bno);
	}

	@Override
	public List<BoardVO> getList() {
		
		return boardMapper.getList();
	}
	
	
}
