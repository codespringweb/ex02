<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page session="false" %>
<%@include file="../includes/header.jsp" %>
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Board Read</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            DataTables Advanced Tables
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
						
								<div class="form-group">
                                    <label>Title</label>
                                    <input class="form-control" name="title" readonly="readonly" value="<c:out value='${board.title}' />">
                                    <p class="help-block">Example block-level help text here.</p>
                                </div>
								<div class="form-group">
                                    <label>Contents</label>
                                    <textarea class="form-control" rows="5" cols="50" name="content" readonly="readonly">
                                     <c:out value="${board.title}" />
                                    </textarea>
                                </div>  
								<div class="form-group">
                                    <label>Writer</label>
                                    <input class="form-control" name="writer" readonly="readonly" value="<c:out value='${board.writer}' />">
                                    <p class="help-block">Example block-level help text here.</p>
                                </div>      
                                <button type="submit" class="btn btn-default"><a href="/board/list">List</a></button>
                                <button type="reset" class="btn btn-default"><a href="/board/modify?bno=<c:out value='${board.bno}' />">Modify</a></button>                          
							
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </div>
        <!-- /#page-wrapper -->
<%@include file="../includes/footer.jsp"%>