<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page session="false" %>
<%@include file="../includes/header.jsp" %>
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Board Modify/Delete</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Board Modify/Delete
                        </div>
                        <!-- /.panel-heading -->
                        <form>
                        <div class="panel-body">
								<div class="form-group">
                                    <label>Modify</label>
                                    <input class="form-control" name="bno" readonly="readonly" value="<c:out value='${board.bno}' />">
                                    <p class="help-block">Example block-level help text here.</p>
                                </div>						
								<div class="form-group">
                                    <label>Modify</label>
                                    <input class="form-control" name="title" value="<c:out value='${board.title}' />">
                                    <p class="help-block">Example block-level help text here.</p>
                                </div>
								<div class="form-group">
                                    <label>Contents</label>
                                    <textarea class="form-control" rows="5" cols="50" name="content" readonly="readonly">
                                     <c:out value="${board.title}" />
                                    </textarea>
                                </div>  
								<div class="form-group">
                                    <label>Writer</label>
                                    <input class="form-control" name="writer" readonly="readonly" value="<c:out value='${board.writer}' />">
                                    <p class="help-block">Example block-level help text here.</p>
                                </div>      
                                <button class="btn btn-default" data-oper="modify">Modify</button>
                                <button class="btn btn-danger" data-oper="remove">Remove</button>  
                                <button class="btn btn-info" data-oper="list">List</button>
                        </div>
                        </form>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
           <script>
           	$(document).ready(function() {
           		var formObj = $('form');
           		$('.btn').click(function(e) {
           			e.preventDefault();
           			var operation = $(this).data("oper");
           			console.log(operation);
           			
           			if (operation === 'list') {
           				self.location = '/board/list';
           			} else if (operation === 'remove') {
           				formObj.attr('action', '/board/remove').attr('method', 'post');
           				formObj.submit();
           			} else if (operation === 'modify') {
           				formObj.attr('action', '/board/modify').attr('method', 'post');
           				formObj.submit();
           			}
           		})
           	})
           </script> 
        </div>
        <!-- /#page-wrapper -->
<%@include file="../includes/footer.jsp"%>