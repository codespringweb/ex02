package org.zerock.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.zerock.domain.BoardVO;

import lombok.extern.log4j.Log4j;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("file:src/main/webapp/WEB-INF/spring/root-context.xml")
@Log4j
public class BoardServiceTests {

	@Autowired
	private BoardService service;
	
	@Test
	public void testPrint() {
		log.info(service);
	}
	
	@Test
	public void testRegister() {
		BoardVO vo = new BoardVO();
		
		vo.setTitle("21 타이틀");
		vo.setContent("21 콘텐츠");
		vo.setWriter("21 작가");

		service.register(vo);
	}
	
	@Test
	public void testRegisterSelectKey() {
		BoardVO vo = new BoardVO();
		vo.setTitle("이름");
		vo.setContent("콘텐츠");
		vo.setWriter("작가");

		Long bno =  service.registerSelectKey(vo);
		
		log.info("insertSelectKey " + bno);
	}
}
