package org.zerock.mapper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.zerock.domain.BoardVO;
import org.zerock.service.BoardService;

import lombok.extern.log4j.Log4j;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("file:src/main/webapp/WEB-INF/spring/root-context.xml")
@Log4j
public class BoardMapperTests {
	
	@Autowired
	private BoardMapper boardMapper;
	

	@Test
	public void testGetList() {
		log.info("--------------------------");
		boardMapper.getList();
	}

	@Test
	public void testInsert() {
		BoardVO vo = new BoardVO();
		vo.setTitle("Test 테스트");
		vo.setContent("Content 테스트");
		vo.setWriter("tester");
		
		boardMapper.insert(vo);
		log.info("after insert " + vo.getBno());
	}
	
	@Test
	public void testInsertSelectKey() {
		BoardVO vo = new BoardVO();
		vo.setTitle("Test 테스트-A");
		vo.setContent("Content 테스트-A");
		vo.setWriter("tester-A");
		
		boardMapper.insertSelectKey(vo);
		log.info("after insertSelectKey " + vo.getBno());
	}
	
	@Test
	public void read() {
		BoardVO vo = null;
		
		vo = boardMapper.read(11L);
		log.info("read " + vo);
	}
	
	@Test
	public void delete() {
		int cnt = boardMapper.delete(1L);
		log.info("delete " + cnt);
	}
	
	@Test
	public void update() {
		BoardVO vo = new BoardVO();
		vo.setBno(2L);
		vo.setTitle("이름");
		vo.setContent("콘텐츠");
		vo.setWriter("작가");

		int cnt = boardMapper.update(vo);
		
		log.info("update " + cnt);
	}
	
}
